# parking-availability

Real-time parking availability

## Nom de l'application : parking-availability

### 2. Technologies utilisées : 
#### 2.1. Serveur
* Java 11
* Java 11
* Spring Boot 2.4.3
* Junit 4.12

#### 2.2. Client
* Angular 10
* leaflet : 1.7.1
* Rxjs: 6.5.5 
* karma 5.0.0 


### 3. Lancement du projet 

#### 3.1. Serveur
 
Pour le lancer en local, ouvrir le dossier "parking_server" dans un IDE puis faire :  
`maven install` pour ramener les dépendances et ensuite executer le projet en tant que projet spring
Le Serveur est alors accessible à l'adresse <http://localhost:8080/> 

#### 3.2. Client
 
Pour le lancer en local, ouvrir un terminal dans le dossier "parking-client" puis faire :  
`npm install`  pour ramener les dépendances 
`npm start --open`  pour lancer le client
Le client est alors accessible à l'adresse <http://localhost:4200/>   

