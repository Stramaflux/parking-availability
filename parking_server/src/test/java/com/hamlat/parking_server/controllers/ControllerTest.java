package com.hamlat.parking_server.controllers;

import com.hamlat.parking_server.services.ParkingService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@WebMvcTest(Controller.class)
class ControllerTest {


    @MockBean
    private ParkingService parkingService;


    @Autowired
    private MockMvc mvc;

    // Méthode pour tester le bon fonctionnement du web service il doit retourner un status 200
    @Test
    public void getParkingTest() throws Exception {


        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parking-api");
        MvcResult result = mvc.perform(requestBuilder).andReturn();

        ResponseEntity<String> expected = new ResponseEntity(HttpStatus.OK);
        assertEquals(expected.getStatusCodeValue(), result.getResponse().getStatus());

    }
}