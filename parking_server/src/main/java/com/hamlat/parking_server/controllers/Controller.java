package com.hamlat.parking_server.controllers;

import com.hamlat.parking_server.services.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;

// Controller pour exposer les services
//@CrossOrigin("*")
@RestController
public class Controller {


    @Autowired
    private ParkingService parkingService;

    // Méthode qui appelle le service parkingService pour récupérer les données et les exposer sur le chemin /parking-api
    @GetMapping(path = "/parking-api")
    public ResponseEntity<String> getParking() throws URISyntaxException {

        return parkingService.getParkingService();
    }
}
