package com.hamlat.parking_server.services;

import com.hamlat.parking_server.config.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;



//Class parking qui implémente l'interface ParkingService et implémente ses méthodes
//@CrossOrigin("*")
@Service
public class ParkingServiceImpl implements ParkingService{

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AppConfig appConfig;

    //Méthode qui requête vers le endpoint api de bordeaux
    @Override
    public ResponseEntity<String> getParkingService() throws URISyntaxException {
        //String baseUrl = "http://data.lacub.fr/geojson?key=9Y2RU3FTE8&typename=st_park_p";
        //String baseUrl = "https://data.mobilites-m.fr/api/findType/json?types=PKG";

        URI uri = new URI(appConfig.getUrl());

        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);
        return result;
    }
}
