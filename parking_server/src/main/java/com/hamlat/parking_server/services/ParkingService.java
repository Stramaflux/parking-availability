package com.hamlat.parking_server.services;

import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;

public interface ParkingService {

    public ResponseEntity<String> getParkingService() throws URISyntaxException;
}
