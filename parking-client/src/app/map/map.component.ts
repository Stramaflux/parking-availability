import { Component, AfterViewInit } from '@angular/core';
import * as L from 'leaflet';
import { MapService } from '../map.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})

// Component qui affiche la car et la position des parkings
export class MapComponent implements AfterViewInit {
  map;
  parkData;
  parking = {
    lat: 44.8349783,
    lng: -0.5793918,
      };

  adress;
  
  // Icone de l'emplacement des parkings 
  smallIcon = new L.Icon({
    iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-icon.png',
    iconRetinaUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-icon-2x.png',
    iconSize:    [25, 41],
    iconAnchor:  [12, 41],
    popupAnchor: [1, -34],
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    shadowSize:  [41, 41]
  });

  constructor(private mapService:MapService) { }

  ngAfterViewInit(): void {
  }

  ngOnInit(): void {
    this.getDataPark();

  }

  // Fonction qui récupère les données des parkings
  getDataPark() {
    this.mapService.getResource().subscribe(
      data=>{
        this.parkData=data;
        this.createMap();
      }, err =>{
        console.log(err)
      }
    )
  }

  // Fonction pour la création de la carte geographique
  createMap() {

    const zoomLevel = 12;

    this.map = L.map('map', {
      center: [this.parking.lat, this.parking.lng],
      zoom: zoomLevel
    });

    const mainLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      minZoom: 10,
      maxZoom: 17,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    mainLayer.addTo(this.map);
      for (let i = 0; i < 86; i++) {
        this.parking.lat=this.parkData.features[i].geometry.coordinates[1];
        this.parking.lng=this.parkData.features[i].geometry.coordinates[0];
        this.adress = this.parkData.features[i].properties.adresse;

      const popupOptions = {
      coords: this.parking,
      text: this.adress,
      open: true
    };
    
    this.addMarker(popupOptions);

      }
    
  }

  // Fonction pour afficher les positions des parkings sur la map
  addMarker({coords, text, open}) {
    const marker = L.marker([coords.lat, coords.lng], { icon: this.smallIcon });
    if (open) {
      marker.addTo(this.map).bindPopup(text).openPopup();
    } else {
      marker.addTo(this.map).bindPopup(text);
    }
  }

}
