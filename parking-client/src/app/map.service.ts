import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

// Service qui requete vers le serveur backend 
export class MapService {

  public host:string="http://localhost:8080/parking-api"
  constructor(private http:HttpClient) { }

  public getResource(){
    return this.http.get(this.host);

  }
}
